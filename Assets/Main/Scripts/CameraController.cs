﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

/// <summary>
/// This script manages the camera
/// </summary>
public class CameraController : MonoBehaviour {

    [Header("The head of the avatar")]
    public Transform head;
    [Header("The cannon gameobject")]
    public GameObject cannon;

    private CannonController cc;
    private WaitForEndOfFrame waitFrame= new WaitForEndOfFrame();


    void Start () {
        
        /*
        if (XRDevice.isPresent) 
            InputTracking.disablePositionalTracking=true; 
        */

        if (cannon)
            cc = cannon.GetComponent<CannonController>();

        StartCoroutine(LockTheCamera());
	}
	
    /// <summary>
    /// This method is used to:
    /// - move the position of the camera on the position of the head
    /// - copy the rotation of the camera to the cannon.
    /// </summary>
    IEnumerator LockTheCamera()
    {
        while (true)
        {
           // Debug.Log("pre transform: "+transform.position+" e head position: "+head.position);
            this.transform.position = head.position;
          //  Debug.Log("post: " + transform.position);

            yield return waitFrame;
            if(cc)
                cc.CopyRotationY(this.transform.rotation);
        }
    }
}

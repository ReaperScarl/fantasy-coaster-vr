﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
/// <summary>
/// Class responsible for the background music
/// </summary>
public class MusicController : MonoBehaviour {

    // Inserted only the ones for the scene.
    public Transform[] musics;
    public Transform[] prelevelmusics;
    [Tooltip("just for the levels")]
    [Range(0,10)]
    public float offset=5;

    private MusicInfo info;

    private Object mManager;
    private GameObject music;
    private int index;

    WaitForSeconds musicDelay= new WaitForSeconds(1);

    void Start () {
        StartTheMusic();
        StartCoroutine(UpdateTheMusic());
    }

   // Every second, it checks if the song is stopped
    public IEnumerator UpdateTheMusic()
    {
        yield return musicDelay;
        while(true)
        {
            if (music = GameObject.FindGameObjectWithTag("music"))
            {
                if (music != null || musics.Length > 0)
                {
                    if (!music.GetComponent<AudioSource>().isPlaying)
                    {
                        GameObject.Destroy(music);
                        index++;
                        mManager = Instantiate(musics[index % musics.Length], transform.position, Quaternion.identity);
                        mManager.name = musics[index % musics.Length].name;

                    }
                }               
            }
            yield return musicDelay;
        }
    }

    // Starts the ambient soundtrack in the scene
    void StartTheMusic()
    {
        GameObject oldMusic;
        oldMusic = GameObject.FindGameObjectWithTag("music");

        if ((SceneManager.GetActiveScene().buildIndex) == 0|| SceneManager.GetActiveScene().buildIndex == SceneManager.GetSceneByName("Finish").buildIndex) // menu scene- finish scene
        {
            if (oldMusic)
            {
                info = oldMusic.GetComponent<MusicInfo>();
                if ((SceneManager.GetActiveScene().buildIndex) != info.level)
                    Destroy(oldMusic.gameObject);
                else
                    return;
            }

            index++;
            mManager = Instantiate(musics[index % musics.Length], transform.position, Quaternion.identity);
            mManager.name = musics[index % musics.Length].name;
        }
        else //every other scene
        {
            if (oldMusic)
            {
                info = oldMusic.GetComponent<MusicInfo>();
                if ((SceneManager.GetActiveScene().buildIndex) != info.level)
                    Destroy(oldMusic.gameObject);
                else
                    return;
            }

            // this is a prelevel music.
            index++;
            mManager = Instantiate(prelevelmusics[index % musics.Length], transform.position + new Vector3(offset, 0, 0), Quaternion.identity,this.transform);
            mManager.name = prelevelmusics[index % musics.Length].name;
        } 
    }

    // Called from the gamemanager when the first target is hit by the player (and the level starts).
    public void StartTheLevelMusic()
    {
        GameObject oldMusic;
        oldMusic = GameObject.FindGameObjectWithTag("music");

        if (oldMusic)
        {
                Destroy(oldMusic.gameObject);
        }

        index++;
        mManager = Instantiate(musics[index % musics.Length], transform.position+new Vector3(offset,0,0), Quaternion.identity,this.transform);
        mManager.name = musics[index % musics.Length].name;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Deprecated.
/// This was used to respawn the weapons after a tot of weapons were taken.
/// </summary>
public class WeaponController : MonoBehaviour {

    public GameObject Weapons;
    private int numOfWeapons;

	// Use this for initialization
	void Start () {
        numOfWeapons=transform.childCount;
        StartCoroutine(RespawnWeapons());
	}

    IEnumerator RespawnWeapons()
    {
        while (this)
        {
            if (transform.childCount < numOfWeapons / 2)
            {
                GameObject.Instantiate(Weapons, transform.parent);
                Destroy(this);
            }

            yield return new WaitForSeconds(5);
        }
    }
}

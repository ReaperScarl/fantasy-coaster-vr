﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class created to do the flexibles buttons (start/retry etc) from the players
/// </summary>
public class ButtonDistanceController : MonoBehaviour {

    [Header ("the transform of the avatar " )]
	public Transform Avatar;
    [Header("the transform of the origin point (the point where it is the kinect camera ")]
    public Transform Origin;



	/// <summary>
	/// we check the distance from the origin and move the button nearer if the distance is too much
	/// </summary>
	public void AdjustTheDistance(){

		// origin is -1 while  avatar is more positive going further from the origin
		float distance = Origin.localPosition.z - Avatar.localPosition.z; 

		if (Mathf.Abs (distance) > 1)
			this.transform.position =new Vector3(transform.position.x,transform.position.y,transform.position.z+(distance+0.5f));
	}
}

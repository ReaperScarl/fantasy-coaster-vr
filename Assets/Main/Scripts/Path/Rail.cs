﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// This script is adapted from a tutorial video on youtube.
/// This calculates the position and the rotation of the rail and passes the information to the mover
/// </summary>
public class Rail : MonoBehaviour {

    [Header("The transfom of the point that startes this rail")]
    public Transform startingPoint;
    [Header("The mover")]
    public Mover mover;
    [Header("The transfom of the point in where there is the game controller")]
    public GameController gameController;

    int currentSegment;
    private int pathDir=1;

    private LinkedList<Transform> nodeList= new LinkedList<Transform>();
    private Transform [] _4nodes= new Transform[4];

    private bool isDirectionSet = false;

    // Use this for initialization
    void Start () {
        StartCoroutine(WaitASec());
    }


    private IEnumerator WaitASec()
    {
        Node node = startingPoint.GetComponent<Node>();
        //Debug.Log(node);
        nodeList.AddLast(startingPoint);
        GetNextLocalPath(node);
        yield return null;
    }

    
    public Vector3 LinearPosition(float ratio)
    {
        Vector3 p1 = _4nodes[1].position;
        Vector3 p2 = _4nodes[2].position;
        return Vector3.Lerp(p1, p2, ratio);
    }

    public Vector3 CatumullPosition(float ratio)
    {
        Vector3 p1, p2, p3, p4;

            p1 = _4nodes[0].position;
            p2 = _4nodes[1].position;
            p3 = _4nodes[2].position;
            p4 = _4nodes[3].position;

        float t2 = ratio * ratio;
        float t3 = t2 * ratio;
        float x = 0.5f * ((2.0f * p2.x)
            + (-p1.x + p3.x) * ratio
            + (2.0f * p1.x - 5.0f * p2.x + 4 * p3.x - p4.x) * t2
            + (-p1.x + 3.0f * p2.x - 3.0f * p3.x + p4.x) * t3);

        float y = 0.5f * ((2.0f * p2.y)
           + (-p1.y + p3.y) * ratio
           + (2.0f * p1.y - 5.0f * p2.y+ 4 * p3.y - p4.y) * t2
           + (-p1.y + 3.0f * p2.y - 3.0f * p3.y + p4.y) * t3);

        float z = 0.5f * ((2.0f * p2.z)
           + (-p1.z + p3.z) * ratio
           + (2.0f * p1.z - 5.0f * p2.z + 4 * p3.z - p4.z) * t2
           + (-p1.z + 3.0f * p2.z - 3.0f * p3.z + p4.z) * t3);

        return new Vector3(x, y, z);
    }

    public Quaternion Orientation(float ratio)
    {
        Quaternion q1 = _4nodes[1].rotation;
        Quaternion q2 = _4nodes[2].rotation;

        return Quaternion.Lerp(q1, q2, ratio);
    }

    /// <summary>
    /// This method will give the length of the node list
    /// </summary>
    /// <returns> the length of the nodelist</returns>
    public int GetLengthNodes()
    {
        return nodeList.Count;
    }

    /// <summary>
    /// This method will get the distance of the currect segment
    /// </summary>
    /// <returns> the distancee between the 2 points of the segment</returns>
    public float GetDistanceCurrentSeg()
    {
        return Vector3.Distance(_4nodes[1].position, _4nodes[2].position); 
    }

    /// <summary>
    /// This method will get the local path until there is an ending segment
    /// </summary>
    /// <param name="node">the node to start the path</param>
    private void GetNextLocalPath(Node node)
    {
        while (!node.endingSegment)
        {
            if(node.numOfDirections==1)
                for(int i=0; i< node.nextNode.Length; i++)
                    if(node.nextNode[i]!=null)
                    {
                      //  Debug.Log("LocalPath - ThisNode "+node.nextNode[i].name);
                        nodeList.AddLast(node.nextNode[i]);
                        node = node.nextNode[i].GetComponent<Node>();
                        break;
                    }
        }
        //Debug.Log("New count: " + nodeList.Count);
    }

    /// <summary>
    /// This method will be called when a lever is pulled.
    /// It will take the direction wanted and than it will use the Get local path method
    /// </summary>
    /// <param name="direction"> the direction- the node to take</param>
    public void NextSegmentFromLever(int direction)
    {
        pathDir = direction;
        if (direction != 1)
            gameController.AddLeverPulled(direction);
        //Needs to find the points for the next segment given the direction from the lever
        Debug.Log("Num dir: " + nodeList.Last());
        Node lastnode =nodeList.Last().GetComponent<Node>();
        Debug.Log("Num dir: "+lastnode.numOfDirections);
        if (lastnode.numOfDirections != 1)
        {
            nodeList.AddLast(lastnode.nextNode[direction]);
            if(lastnode.nextNode[direction]!=null)
                GetNextLocalPath(lastnode.nextNode[direction].GetComponent<Node>());
        }
        isDirectionSet = true;
    }


    /// <summary>
    /// This method will give the first 4-points segment to work with
    /// </summary>
    public void FirstSegment()
    {
        //Debug.Log("Count: "+nodeList.Count);
        currentSegment = 0;
        _4nodes[0] = nodeList.First();
        _4nodes[1] = _4nodes[0];
        _4nodes[2] = nodeList.ElementAt(1);
        _4nodes[3] = nodeList.ElementAt(2);
    }

    /// <summary>
    /// This method will give the 4-points segment to work with after the first one
    /// </summary>
    public void NextSegment()
    {
        currentSegment++;
       // Debug.Log("Node Count : " + nodeList.Count + " and current: " + currentSegment);
        if(currentSegment<nodeList.Count-3)
        {
            for(int i=0; i<4; i++)
                _4nodes[i] = nodeList.ElementAt(currentSegment+i-1);
        }
        else
        {
            //Debug.Log("Count Before the last: " +nodeList.Count +" and current: "+currentSegment);
            if (currentSegment < nodeList.Count - 1)
            {
                //Debug.Log("before the last element");
                if (nodeList.ElementAt(currentSegment + 1).GetComponent<Node>().isLastNode)
                {
                    _4nodes[0] = nodeList.ElementAt(currentSegment - 1);
                    _4nodes[1] = nodeList.ElementAt(currentSegment);
                    _4nodes[2] = nodeList.ElementAt(currentSegment + 1);
                    _4nodes[3] = _4nodes[2];
                }
                else
                {
                    //Debug.Log("Should go for le default direction");
                    if (!isDirectionSet || pathDir != 1)
                    {
                        NextSegmentFromLever(1);
                        pathDir = 1;
                        isDirectionSet = false;
                    }
                        
                    for (int i = 0; i < 4; i++)
                        _4nodes[i] = nodeList.ElementAt(currentSegment + i - 1);
                }
            }
            else
            {
                //Debug.Log("Current node last: " + nodeList.ElementAt(currentSegment).GetComponent<Node>());
                if (nodeList.ElementAt(currentSegment).GetComponent<Node>().isLastNode)
                {
                    mover.Finish();
                    if (nodeList.ElementAt(currentSegment).GetComponent<Node>().isFinishNode)
                        gameController.Win();
                    else
                        gameController.Lost();
                }
                else
                {
                    Debug.Log("Current node last: " + nodeList.ElementAt(currentSegment).GetComponent<Node>());
                }

               // return;
            }    
        }
       // Debug.Log("Current node: "+_4nodes[1]);
    }

    /// <summary>
    /// This method gives the acceleration of the first point of the current segment
    /// </summary>
    /// <returns> the acceleration in the node</returns>
    public float GetNewAcceleration()
    {
        return _4nodes[1].GetComponent<Node>().changevelocitySegment;
    }

}

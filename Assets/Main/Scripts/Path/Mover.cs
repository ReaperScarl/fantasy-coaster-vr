﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayMode
{
    Linear,
    Catmull
}

/// <summary>
/// Script adapted from a tutorial video on youtube.
/// This is used to manage the movement of the rail.
/// </summary>
public class Mover : MonoBehaviour {

    [Header("The rail component object ")]
    public Rail rail;
    [Header("The playmode between points for the movement")]
    public PlayMode mode;
    [Header("The meters to do usually in the unit of time (divided by the distance of the current segment) ")]
    public float metersToDoInTimeToTrans = 20;
    [Space(10)]
    [Header("is The mover starting with an attack? false to start after a wait")]
    public bool startWithAttack = false;
    [Header("How much time to wait before start")]
    public float waitBeforeStart = 15;
    [Space(10)]
    [Header("Is the rail accelerating over time? false to not have accelerations")]
    public bool accelerate;
    [Header("Time after witch it will add an acceleration (based on the current segment)")]
    public float timeToAddAcceleration = 0.5f;
   

    private int currentSeg=0;
    private float transition;
    private bool isCompleted;
    private float timeTotrans = 2f;
    private float distance = 0; // the distance of the current segment
    private float newAddictionToAccelerate = 1;

    private AudioSource minecart;

    private void Start()
    {
        if(!startWithAttack)
            StartCoroutine(UpdateTheMover());

        minecart = GetComponent<AudioSource>();
    }

    IEnumerator UpdateTheMover()
    {
        if(!startWithAttack)
            yield return new WaitForSeconds(waitBeforeStart);

        if (accelerate)
            StartCoroutine(Accelerate());

        FirstSegment();
       
        distance = rail.GetDistanceCurrentSeg();

        minecart.Play();
        while (rail && !isCompleted)
        {
            Play();
            yield return null;
        }
    }

    /// <summary>
    /// Accelerate the kart
    /// </summary>
    IEnumerator Accelerate()
    {
        while (!isCompleted)
        {
            metersToDoInTimeToTrans += newAddictionToAccelerate;
            yield return new WaitForSeconds(timeToAddAcceleration);
        }
    }

    /// <summary>
    /// Called each frame to change the position and the rotation of the kart
    /// </summary>
    private void Play()
    {
        //float distance = rail.getDistanceCurrentSeg(currentSeg);
        transition += Time.deltaTime * 1 / timeTotrans*(metersToDoInTimeToTrans/distance);
        
        if (transition > 1)
        {
            transition = 0; // to move
            currentSeg++;
            //Debug.Log("before NextSegment!");
            NextSegment();
            distance = rail.GetDistanceCurrentSeg();
            if (isCompleted||currentSeg > rail.GetLengthNodes()-1)
            {
                isCompleted = true;
                Debug.Log("Completed!");
                return;
            }
        }
        transform.position = PositionOnRail(transition, mode); // To change
        transform.rotation = rail.Orientation(transition); // To change
    }

    /// <summary>
    /// Check which mode is used, the ration in the segment and returns the position of the player
    /// </summary>
    /// <param name="ratio"> the ratio between 0 and 1 of the player </param>
    /// <param name="mode"> the mode of the movement</param>
    /// <returns> the position of the kart</returns>
    public Vector3 PositionOnRail(float ratio, PlayMode mode)
    {
        switch (mode)
        {
            default:
            case PlayMode.Linear:
                return rail.LinearPosition(ratio);
            case PlayMode.Catmull:
                return rail.CatumullPosition(ratio);
        }
    }

   

    private void FirstSegment()
    {
        rail.FirstSegment();
    }

    /// <summary>
    /// get the new segment and also will change the acceleration
    /// </summary>
    private void NextSegment()
    {
        rail.NextSegment();
        if(accelerate)
            newAddictionToAccelerate = rail.GetNewAcceleration();
    }
   
    /// <summary>
    /// Called when the kart has ended the journey
    /// </summary>
    public void Finish()
    {
        minecart.Stop();
        isCompleted = true;
    }

    /// <summary>
    /// Called to start the coaster with an attack
    /// </summary>
    public void StartTheCoaster()
    {
        if(startWithAttack)
            StartCoroutine(UpdateTheMover());
    }
}

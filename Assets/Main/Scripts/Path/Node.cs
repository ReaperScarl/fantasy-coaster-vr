﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif


#if UNITY_EDITOR
[ExecuteInEditMode]
#endif
/// <summary>
/// This script has all the informations about a node.
/// </summary>
public class Node : MonoBehaviour {

    [Header("the new acceleration for the segment ahead")]
    public float changevelocitySegment = +5;
    [Header("The nodes ahead")]
    [Tooltip("0=left, 1=center, 2=right")]
    public Transform[] nextNode = new Transform[3]; // left, center, right
    [Header("number of available directions ")]
    [Header("(usually 1, the same number of not null element in NextNode).")]
    public int numOfDirections=1;
    [Header("Is this node an ending segment?")]
    [Tooltip("the ending segment is when the the number of directions is different from 1.\n For example there are 2 ways or it is the last node")]
    public bool endingSegment;
    [Header("Is this node a starting segment?")]
    public bool startingSegment;
    [Header("Is this node the last node?")]
    [Tooltip("The last node is when there are not nodes ahead of this one")]
    public bool isLastNode;
    [Header("Is this node the finishing node?")]
    [Tooltip("The finishing node is when there are not nodes ahead of this one, but it is also a win node")]
    public bool isFinishNode;

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        for (int i = 0; i < nextNode.Length; i++)
        {
            if(nextNode[i]!=null)
                Handles.DrawLine(transform.position, nextNode[i].position);
        }
    }
#endif

}


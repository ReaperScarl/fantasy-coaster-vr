﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is responsible for the interaction with the Lever mechanism
/// </summary>
public class Lever : MonoBehaviour {

    [Header ("Can be put to 0 (left) or 2 (right)")]
    [Range(0,2)]
    public int direction;

    private Animation anim;
    private AudioSource audioS;

	void Start () {
        anim = GetComponent<Animation>();
        audioS = GetComponent<AudioSource>();
	}
	

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.GetComponent<TagForHands>()!=null)
        {
            anim.Play("Pulled_Lever");
            audioS.Play();
            GameObject.Find("MovementPoints").GetComponent<Rail>().NextSegmentFromLever(direction);
            this.GetComponent<ParticleSystem>().Play();
        }
    }

}

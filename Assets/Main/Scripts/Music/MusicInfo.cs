﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script will store the level in which this music is used.
/// Also it will store the rotation of the music in the air when playing
/// </summary>
public class MusicInfo : MonoBehaviour {

    [Header("Level of this music")]
    public int level;
    [Range(0, 100)]
    public float rotation=5;

    private void Start()
    {
        StartCoroutine(Rotate());
    }

    IEnumerator Rotate()
    {
        if (transform.parent != null)
        {
            while (this)
            {
                this.transform.parent.Rotate(new Vector3(0, 1, 0), rotation * Time.deltaTime);
                yield return null;
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script manages the hit of the weapons in the level
/// </summary>
public class WeaponHit : MonoBehaviour {

    //public AudioClip metalHit;
    //public AudioClip ConcreteHit;
    [Header("This array contains the clips for the different hits.")]
    [Header("0 - Concrete hit clip for the terrain")]
    [Header("1 - A cool clip for the target")]
    public AudioClip[] hits;

    private AudioSource source;

	// Use this for initialization
	void Start () {
        source = GetComponent<AudioSource>();
	}
	
    void OnCollisionEnter(Collision coll)
    {
        // because there are just these 2 
        //- could be improved with the sound for the wood or the metal when hitting the rails.
        //Debug.Log("Collision");
        if (coll.transform.tag == "target")
            source.clip = hits[1];
        else
            source.clip = hits[0];
        source.spatialize = true;
        source.Play();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script is made just to make some zombie wander around for no reason
/// </summary>
public class ZombieBehaviour : MonoBehaviour {

    public GameObject pointOfInterest;
    public float velocity=0.5f;
    public float timeAlive = 20;

   // private Rigidbody myBody;
    // Use this for initialization
    void Start () {

        StartCoroutine(Move());
        StartCoroutine(DieForLoneliness());
	}
	
	
    
    IEnumerator Move()
    {
        while (transform)
        {
            Vector3 dir = pointOfInterest.transform.position - transform.position;
            float distanceThisFrame = velocity * Time.deltaTime;
            if (pointOfInterest)
            {
                transform.LookAt(pointOfInterest.transform);
                 transform.position+=transform.forward*distanceThisFrame;
            }
            yield return null;
        }
    }

    IEnumerator DieForLoneliness()
    {
        yield return new WaitForSeconds(timeAlive);
        Destroy(this.gameObject);
    }

}

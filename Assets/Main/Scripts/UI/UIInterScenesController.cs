﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// This script manages the UI button in the game
/// </summary>
public class UIInterScenesController : MonoBehaviour {

    public enum Choices
    {
        Quit,
        Start,
        Retry,
        MainMenu,
        NextLevel
    }
    [Header("The function of this button")]
    public Choices choice;

    private ImageFade img;

    private void OnTriggerEnter()
    {
        if(img==null)
            img = GameObject.Find("ReverseSphere").GetComponent<ImageFade>();

        switch (choice)
        {
            case Choices.Quit:
                //Debug.Log("Quit");
                Application.Quit();
                break;

            case Choices.Retry:
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                img.StartTheFade(false, 1);
                break;

            case Choices.Start:
                //Debug.Log("Start");
                SceneManager.LoadScene(1);
                img.StartTheFade(false, 1);
                break;

            case Choices.MainMenu:
                SceneManager.LoadScene(0);
                img.StartTheFade(false, 1);
                break;

            case Choices.NextLevel:
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
                img.StartTheFade(false, 1);
                break;
        }
    }
}

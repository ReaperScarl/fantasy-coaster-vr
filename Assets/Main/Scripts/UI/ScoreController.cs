﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This controls the score textes inside the Finish and Main Menu scenes
/// </summary>
public class ScoreController : MonoBehaviour {

    /// 4 stats
    /// 0: target hitted
    /// 1: levers pulled
    /// 2: number of bullets shooted
    /// 3: accuracy
    /// 4: hats collected
    /// 5: total

    public Text [] stats;


	void Start () {

        ShowScore();
    }


    void ShowScore()
    {
        int targets= PlayerPrefs.GetInt("targets");
        int bullets= PlayerPrefs.GetInt("bullets");
        int levers= PlayerPrefs.GetInt("levers");
		int hats= PlayerPrefs.GetInt("hats");

        float accuracy;
        accuracy = (bullets == 0) ? 0 : (float)(targets / bullets);


        stats[0].text = "" + targets;
        stats[1].text = "" + levers;
        stats[2].text = "" + bullets;
        stats[3].text = "" + accuracy;
		stats[4].text = "" + hats;
        stats[5].text = "" + (targets + levers - (bullets / 20));
    }
}

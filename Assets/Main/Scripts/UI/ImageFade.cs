﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// This is the script that manages the fade in-fade out of the fade element
/// </summary>
public class ImageFade : MonoBehaviour
{

    private Image img;
    private MeshRenderer mRenderer;

    private void Start()
    {
        img = GetComponent<Image>();
        if (!img)
            mRenderer = GetComponent<MeshRenderer>();

    }

    /// <summary>
    /// This method will start the fade.
    /// It is called by the game controller
    /// </summary>
    /// <param name="toFade"> is to Fade in? true=yes</param>
    /// <param name="timeToFade">the time to fade</param>
    public void StartTheFade(bool toFade, float timeToFade)
    {
        StartCoroutine(FadeImage(toFade, timeToFade));
    }

    IEnumerator FadeImage(bool fadeAway, float timeToFade)
    {
       
        if (fadeAway)  // fade from opaque to transparent
        {
            // loop over 1 second backwards
            for (float i = timeToFade; i >= 0; i -= Time.deltaTime)
            {
                if(img)  // set color with i as alpha
                    img.color = new Color(img.color.r, img.color.g, img.color.b, i);
                else
                    mRenderer.material.color= new Color(mRenderer.material.color.r, mRenderer.material.color.g, mRenderer.material.color.b, i);
                yield return null;
            }
        }
        else  // fade from transparent to opaque
        {
            // loop over 1 second
            for (float i = 0; i <= timeToFade; i += Time.deltaTime)
            {
                if (img)  // set color with i as alpha
                    img.color = new Color(img.color.r, img.color.g, img.color.b, i);
                else
                    mRenderer.material.color = new Color(mRenderer.material.color.r, mRenderer.material.color.g, mRenderer.material.color.b, i);
                yield return null;
            }
        }
    }


}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This script will make disappear the text
/// </summary>
public class TextManager : MonoBehaviour {

    [Header("how many seconds before make disappear the text")]
    public float timeToDisappear=3;

    //public int maxTextSize = 9;
    //private int startingSize;
    private Text text;
    private WaitForSeconds delay;

	// Use this for initialization
	void Start () {
        text = GetComponent<Text>();
        //startingSize = text.fontSize;
        delay = new WaitForSeconds(timeToDisappear);

    }
	
    /// <summary>
    /// Called by other methods to make disappear an element
    /// </summary>
    public void BiggerAndDesappear()
    {
        this.gameObject.SetActive(true);
        StartCoroutine(BiggerTextAndDesappear());
    }


    IEnumerator BiggerTextAndDesappear()
    {
        yield return delay;
        this.gameObject.SetActive(false);
       //text.fontSize=startingSize;
    }

}

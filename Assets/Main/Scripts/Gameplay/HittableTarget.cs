﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This manages the hittable target elements
/// </summary>
public class HittableTarget : MonoBehaviour {

    //public AudioClip explosion;

    private AudioSource source;
    public GameObject explosion;
    
	void Start () {
        source = GetComponent<AudioSource>();
	}

    private void OnTriggerEnter(Collider other)
    {
        GameObject.Find("GameController").GetComponent<GameController>().Targethitted();
        source.Play();
        
        GameObject exp=Instantiate(explosion);
        exp.transform.position = this.transform.position;
        exp.GetComponent<ParticleSystem>().Play();
		StartCoroutine (DestroyWithDelay ());
		this.GetComponent<Renderer> ().enabled = false;
        this.GetComponent<MeshCollider>().enabled = false;
    }


	IEnumerator DestroyWithDelay(){
		yield return new WaitForSeconds (1);
		Destroy (this.gameObject);
	}

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This manages the hat element
/// </summary>
public class CollectibleHat : MonoBehaviour {

    private AudioSource source;

    // Use this for initialization
    void Start () {
        source = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        GameObject.Find("GameController").GetComponent<GameController>().HatCollected();
        source.Play();
		StartCoroutine (DestroyWithDelay ());
		this.GetComponent<Collider> ().enabled = false;
        this.GetComponent<SpriteRenderer>().enabled = false;
    }

	IEnumerator DestroyWithDelay(){
		yield return new WaitForSeconds (1);
		Destroy (this.gameObject);
	}
}

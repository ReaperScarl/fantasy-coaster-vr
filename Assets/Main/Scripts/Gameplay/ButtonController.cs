﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class manages the button of the cannon. 
/// This should:
///   - "animate" the button when pressed 
///   - Change color if the cannon is loaded or not
/// </summary>
public class ButtonController : MonoBehaviour {

    public float distanceofAnimation=0.15f;
    public float reactionOfPression = 0.5f;
    public float loadingTime=1;
    [Header("The cannon object with the cannon controller")]
    public GameObject cannon;
    
    bool isLoaded=true;

    private Color loaded = Color.green;
    private Color inLoad = Color.red;
    private Material material;
    private CannonController cc;
    private AudioSource aS;

    void Start () {
        material = this.GetComponent<MeshRenderer>().material;
        cc = cannon.GetComponent<CannonController>();
        material.color = loaded;
        aS = GetComponent<AudioSource>();
	}

    private void OnTriggerEnter(Collider other)
    {
        StartCoroutine(buttonPressed());
        if (isLoaded)
        {
            ChangeColor(true);
            isLoaded = false;
            cc.Fire();
        }
        StartCoroutine(countdown());
    }

    void ChangeColor(bool isloading)
    {
        if(isloading)
            material.color = inLoad;
        else
            material.color = loaded;
    }

    IEnumerator buttonPressed()
    {
        aS.Play();
        this.transform.localPosition=new Vector3(this.transform.localPosition.x, this.transform.localPosition.y - distanceofAnimation, this.transform.localPosition.z);
        yield return new WaitForSeconds(0.5f);
        this.transform.localPosition = new Vector3(this.transform.localPosition.x, this.transform.localPosition.y + distanceofAnimation, this.transform.localPosition.z);
    } 


    IEnumerator countdown()
    {
        yield return new WaitForSeconds(loadingTime);
        isLoaded = true;
        ChangeColor(false);
    }
}

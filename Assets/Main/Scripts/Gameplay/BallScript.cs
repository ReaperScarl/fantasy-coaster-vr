﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is the script that manages the ball element
/// It will be automatically destroyed after 5 seconds
/// - In a next release it could be put in an object pool instead of destroy it
/// </summary>
public class BallScript : MonoBehaviour {

    float TOL = 5;

	// Use this for initialization
	void Start () {
        StartCoroutine(Destroy());
	}
	
	
    IEnumerator Destroy()
    {
        yield return new WaitForSeconds(TOL);
        Destroy(this.gameObject);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Deprecated.
/// This script was used to keep track of the Open/close state of the hand
/// </summary>
public class TagForHands : MonoBehaviour {

    public string handState;

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Deprecated.
/// This script was used to make disappear the body color of the body
/// </summary>
public class BodyDesappear : MonoBehaviour {

	// Use this for initialization
	void Start () {

        Material[] newmaterials = this.GetComponent<SkinnedMeshRenderer>().materials;

        foreach(Material newmaterial in newmaterials)
        {
          //  Debug.Log(newmaterial.name);
            if(newmaterial.name!="Hand_R"&& newmaterial.name != "Hand_L")
            {
                newmaterial.color = Color.clear;
            }
        }

        this.GetComponent<SkinnedMeshRenderer>().materials = newmaterials;
    }
	

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Deprecated.
/// This script was used to launch manage the item in the hand and than to launch it.
/// </summary>
public class HandKeepBehaviour : MonoBehaviour {

    public bool hasAlreadyAnObject;

    Transform objectInHand;

    public bool AddATransform(Transform t)
    {
        if (!hasAlreadyAnObject)
        {
            hasAlreadyAnObject = true;
            objectInHand = t;
            t.parent = this.transform;
            EnableGravity(false);
            return true;
        }
        return false;
        
    }

    public void LaunchObject()
    {
        //Launch object in hand
        Debug.Log("Object: " + objectInHand);
        Debug.Log("object.parent: " + objectInHand.parent);
        if (objectInHand.parent)
            objectInHand.parent = null;
        EnableGravity(true);
        objectInHand.GetComponent<Rigidbody>().AddForce(this.GetComponent<VelocityController>().GetDirection()* this.GetComponent<VelocityController>().velocity*100);
        objectInHand.GetComponentInChildren<ChangeColor>().isTaken = false;
        //Debug.Log("Is taken from hand: " + objectInHand.GetComponent<ChangeColor>().isTaken);
        hasAlreadyAnObject = false;
    }


    private void EnableGravity(bool really)
    {
        if (really)
        {
            objectInHand.GetComponent<Rigidbody>().isKinematic = false;
            objectInHand.GetComponent<Rigidbody>().useGravity = true;
            
        }
        else
        {
            objectInHand.GetComponent<Rigidbody>().useGravity = false;
            objectInHand.GetComponent<Rigidbody>().isKinematic = true;
        }
    }
}

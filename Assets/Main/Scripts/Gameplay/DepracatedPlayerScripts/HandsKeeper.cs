﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Deprecated.
/// This script was used to manage the references on the left and right hands from other scripts
/// </summary>
public class HandsKeeper : MonoBehaviour {

    public Transform LeftHand;
    public Transform RightHand;
}

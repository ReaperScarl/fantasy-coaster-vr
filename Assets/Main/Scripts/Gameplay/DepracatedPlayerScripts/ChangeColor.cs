﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Deprecated.
/// This should have been used to color the hands of red when the hand is closed and of green when they are open.
/// It also managed the interaction with the other elements
/// </summary>
public class ChangeColor : MonoBehaviour {

    static Color transparentRed = new Color(1, 0, 0, 90);
    static Color transparentGreen = new Color(0, 1, 0, 90);
    static Color transparent = new Color(0, 0, 0, 0);

    public bool isOpenWhenEntering;
    public bool isTaken = false;


    private void Start()
    {
        GetComponent<Renderer>().material.color = transparent;
    }


    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log(other);
        // Debug.Log(other.GetComponent<TagForHands>().handState);
        if (other.GetComponent<TagForHands>())
        {
            if (other.GetComponent<TagForHands>().handState == "Closed")
            {
                GetComponent<Renderer>().material.color = transparentRed;
                isOpenWhenEntering = false;
            }
            if (other.GetComponent<TagForHands>().handState == "Open")
            {
                GetComponent<Renderer>().material.color = transparentGreen;
                isOpenWhenEntering = true;
            }
        }
       
    }

    private void OnTriggerStay(Collider other)
    {
       // Debug.Log("Stay "+other);

        if (!isOpenWhenEntering)
            return;
        if (other.GetComponent<TagForHands>())
        {
            if (other.GetComponent<TagForHands>().handState == "Closed")
            {
                GetComponent<Renderer>().material.color = transparentRed;
                //Has to take the object
                if (!isTaken)
                {
                    if (other.enabled)
                    {
                        //Debug.Log("Other: " + other + " transform " + transform);
                        isTaken = other.GetComponent<HandKeepBehaviour>().AddATransform(transform.parent);
                    }

                }
                //Debug.Log("Is Taken: " + isTaken);
            }

            if (other.GetComponent<TagForHands>().handState == "Open" && isTaken)
            {
                other.GetComponent<HandKeepBehaviour>().LaunchObject();
            }
        }
           

    }
    private void OnTriggerExit(Collider other)
    {
        GetComponent<Renderer>().material.color = transparent;
    }

}

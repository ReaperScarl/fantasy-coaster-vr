﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is responsible for the management of the Cannon.
/// This should:
///  - Fire when the button is hit.
///  - Move when the player touch it (just in a local rotation)
/// </summary>
public class CannonController : MonoBehaviour {

    public GameObject ball;
    [Range(10,4000)]
    [Header("The force strength to multiply for the forward of the spawn point")]
    public float ShotStrength=20;
    
    public float MaxLimitY = -5;
    public float MinLimitY = -45;

    private Transform SpawnPoint;
    private int bullets=0;

	// Use this for initialization
	void Start () {
        SpawnPoint = transform.Find("SpawnPoint");
    }

    /// <summary>
    /// Thi method is called to spawn the projectile and shoot it with shot strengh
    /// </summary>
    public void Fire()
    {
        GameObject projectile = Instantiate(ball);
        projectile.transform.position = SpawnPoint.position;
        projectile.GetComponent<Rigidbody>().AddForce(SpawnPoint.forward*ShotStrength);
        bullets++;
    }

    /// <summary>
    /// This is called by the head controller to copy the movement and rotation of the player's head
    /// </summary>
    /// <param name="quat"> the quaternion of the camera player</param>
    public void CopyRotationY(Quaternion quat)
    {
        transform.SetPositionAndRotation(this.transform.position, quat);
    }

    /// <summary>
    /// Returns the number of bullets shot from the start of the scene
    /// </summary>
    /// <returns> the number of bullets shot</returns>
    public int getBullets()
    {
        return bullets;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This is the Script that will make hit the head when touches the wood
/// </summary>
public class Obstacle : MonoBehaviour {


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "head")
        {
            Debug.Log("You didn't pass it");
            GameObject.Find("GameController").GetComponent<GameController>().Lost();
        }

    }
}

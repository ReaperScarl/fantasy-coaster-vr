﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

/// <summary>
/// This script is the game controller of the level
/// </summary>
public class GameController : MonoBehaviour {

    [Header ("The canvas with the text with the hint to start")]
    public GameObject CanvasStart;
    [Header("The button to retry")]
    public GameObject Retry;
    [Header("The button to go to the next level")]
    public GameObject NextLevel;
    [Header("The button to go back to the main menu")]
    public GameObject MainMenu;
   
    [Space(10)]
    [Header("The text with the overall score")]
    public Text scoreText;
    [Header("The text about the target score")]
    public Text targetHittedText;
    [Header("The text about the lever score")]
    public Text LeverPulledText;
    [Header("The text about the hat score")]
    public Text HatCollectedText;
    [Space(10)]
    [Header("The text in which will be written the feedback when the lever is pulled")]
    public Text LeverInfo;
    [Header("The text in which will be written the feedback about win/lost")]
    public Text Lost_Win;

    [Space(10)]
    [Header("The background sphere to do the fade in/out")]
    public GameObject backGround;
    [Header("The time to fade the background. Doubled for the long blink")]
    public float timeToFade = 1;
    [Header("The gameobject with the music controller")]
    public GameObject musicController;

    // Should store the infos about the game.
    int targetHitted=0;
    int leverPulled = 0;
    int hatCollected = 0;

    private MusicController mC;

    private WaitForSeconds fadeDelay;
    //Fade out
    void Start () {
        StartCoroutine(LongBlink());
        // StartCoroutine(Blink());
        mC=musicController.GetComponent<MusicController>();
        fadeDelay = new WaitForSeconds(timeToFade);
    }

    public void Targethitted()
    {
        if (targetHitted == 0) // first target
        {
            StartCoroutine( LongBlink());
            GameObject.Find("RailRaider").GetComponent<Mover>().StartTheCoaster();
            CanvasStart.gameObject.SetActive(false);
            scoreText.gameObject.SetActive(true);
            scoreText.color = Color.white;
            targetHittedText.gameObject.SetActive(true);
            LeverPulledText.gameObject.SetActive(true);
            HatCollectedText.gameObject.SetActive(true);
            mC.StartTheLevelMusic();
        } 
        targetHitted++;
        targetHittedText.text = "TargetHitted: " + targetHitted;
        scoreText.text = "Score: " + (targetHitted + leverPulled+hatCollected);
    }

    public void HatCollected()
    {
        hatCollected++;
        HatCollectedText.text = "Hat collected: " + hatCollected;
        scoreText.text = "Score: " + (targetHitted + leverPulled+hatCollected);

    }

    /// <summary>
    /// Called when the player arrives at the end of the road
    /// </summary>
    public void Win()
    {
        StartCoroutine(LongBlink());
        Retry.gameObject.SetActive(true);
        NextLevel.gameObject.SetActive(true);
        MainMenu.gameObject.SetActive(true);
        Lost_Win.gameObject.SetActive(true);
        Lost_Win.text = "Nice! You passed the level!";
        // Fade and results
        TransferScore();
    }

    public void Lost()
    {
        StartCoroutine(LongBlink());
        Retry.gameObject.SetActive(true);
        MainMenu.gameObject.SetActive(true);
        //Fade and results
        Lost_Win.gameObject.SetActive(true);
        Lost_Win.text = "Ouch, you failed!";
        GameObject.Find("RailRaider").GetComponent<Mover>().metersToDoInTimeToTrans = 0;
        TransferScore();
    }

    public void AddLeverPulled(int direction)
    {
        leverPulled++;
        LeverPulledText.text = "Levers Pulled: " + leverPulled;
        scoreText.text = "Score: " + (leverPulled + targetHitted+hatCollected);
        LeverInfo.text = "You pulled the"+'\n';
        LeverInfo.text += (direction == 0) ? " left" : " right";
        LeverInfo.text += " lever";
        LeverInfo.transform.GetComponent<TextManager>().BiggerAndDesappear();
    }

    IEnumerator Blink()
    {
        backGround.GetComponent<ImageFade>().StartTheFade(true, timeToFade);
        while (this)
        {
            backGround.GetComponent<ImageFade>().StartTheFade(false, timeToFade);
            yield return fadeDelay;
            backGround.GetComponent<ImageFade>().StartTheFade(true, timeToFade);

        }
    }

    IEnumerator LongBlink()
    {
        backGround.GetComponent<ImageFade>().StartTheFade(false, timeToFade);
        yield return fadeDelay;
        yield return fadeDelay;
        backGround.GetComponent<ImageFade>().StartTheFade(true, timeToFade);
    }

    /// <summary>
    /// This saves the scores in the player prefs when the player loses or wins
    /// </summary>
    void TransferScore()
    {
        PlayerPrefs.SetInt("levers", PlayerPrefs.GetInt("levers") + leverPulled);
        PlayerPrefs.SetInt("targets", PlayerPrefs.GetInt("targets") + targetHitted);
        PlayerPrefs.SetInt("bullets", PlayerPrefs.GetInt("bullets")+GameObject.Find("Cannon").GetComponent<CannonController>().getBullets());
		PlayerPrefs.SetInt("hats", PlayerPrefs.GetInt("hats") + hatCollected);
    }
}

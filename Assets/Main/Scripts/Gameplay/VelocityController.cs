﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Deprecated. This Script was responsible to give the velocity of the hand when the hand passed to the open state.
/// Now it is not used anymore.
/// </summary>
public class VelocityController : MonoBehaviour {

    public float velocity=0;
    public float reactionTime=0.2f;
    public float minDiff = 0.01f;
    public float multiplier = 5;
    Vector3 position;
    public Transform cam;

    private WaitForSeconds secToWait;


	// Use this for initialization
	void Start () {
       
        position = transform.position;
        StartCoroutine(CalculateVelocity());
        secToWait= new WaitForSeconds(reactionTime);
    }
	

	IEnumerator CalculateVelocity () {
        while (true)
        {
            yield return secToWait;
            if (this.enabled)
            {
                velocity = Vector3.Distance(position, transform.position);
               
                position = transform.position;
                if (velocity < minDiff)
                    velocity = 0;
                else
                    velocity=velocity* multiplier;

                //Debug.Log(transform.name + ": " + velocity);
            }
        }
    }

    public Vector3 GetDirection()
    {
            return cam.forward+(new Vector3(0,0.5f,0));
    }
}

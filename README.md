# READ ME

This is a university project for the subject "Virtual Reality" made in Unity and that uses Kinect and Oculus together. 
This project has been developed by Francesco Scarlata.

The contacts will be shown below.

## What is this repository for?

### Quick summary
This is a roller coaster in VR that uses the Kinect device to interact with the world.

There are 4 main mechanics prototyped: 

 - Targets to shoot
 - Levers to pull
 - Avoid trunks on the road
 - Hats to take

# How do I get set up?

### Summary of set up
Download this project and open it with Unity (version 2017.2.0+).

After it is opened, you can build it going in "Settings" and choose the "Standalone mode". 

Go to Edit > Project Settings > Player and check the box to enable "Virtual Reality Supported".

Install Kinect and Oculus devices properly.

If you want just the build without open Unity, contact me (See below the contact) and i'll send it to you.

# Who do I talk to?

##### Repo owner:       Francesco Scarlata

##### E-mail:           francescoscarl93@gmail.com

##### Linkedin page:   https://www.linkedin.com/in/francescoscarlata/